package com.networked.assets.jsonPlaceholder.exception;

import java.util.function.Supplier;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.networked.assets.jsonPlaceholder.model.Post;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class SavingPostsException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public SavingPostsException(String msg) {
		super(msg);
	}

	public static Supplier<SavingPostsException> ofPost(Post post) {
		return () -> new SavingPostsException("Error while saving post: " + post.toString());
	}
}
