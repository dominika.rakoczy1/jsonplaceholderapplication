package com.networked.assets.jsonPlaceholder.exception;

import java.security.InvalidParameterException;
import java.util.function.Supplier;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.networked.assets.jsonPlaceholder.model.Post;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CorruptedPostsException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public CorruptedPostsException(String msg) {
		super(msg);
	}

	public static Supplier<CorruptedPostsException> ofPost(Post post) {
		return () -> new CorruptedPostsException("Post comes corrupted, stop processing:"+ post.toString());
	}

	public static Supplier<CorruptedPostsException> ofPosts() {
		return () -> new CorruptedPostsException("Posts data structure has been change or data comes corrupted.");
	}
}
