package com.networked.assets.jsonPlaceholder.ctrl;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.networked.assets.jsonPlaceholder.service.PostService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/posts")
@AllArgsConstructor
public class PostCtrl {

	private final PostService postService;

	@GetMapping
	// TODO change to POST
	public void saveAllPosts() {
		postService.saveAllPosts();
	}

}
