package com.networked.assets.jsonPlaceholder.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networked.assets.jsonPlaceholder.exception.CorruptedPostsException;
import com.networked.assets.jsonPlaceholder.exception.SavingPostsException;
import com.networked.assets.jsonPlaceholder.model.Post;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PostService {
	private final RestTemplate restTemplate;

	private final ObjectMapper objectMapper;

	private final String postJsonDirectory;

	private final String postUri;

	@Autowired
	public PostService(RestTemplate restTemplate, ObjectMapper objectMapper,
			@Value("${post.json.directory}") String postJsonPath, @Value("${post.uri}") String postUri) {

		this.restTemplate = restTemplate;
		this.objectMapper = objectMapper;
		this.postJsonDirectory = postJsonPath;
		this.postUri = postUri;

		validateDirectory();
	}

	public void saveAllPosts() {
		log.info("Start processing post from api");
		readPostFromUri().forEach(p -> savePostJson(p));
		log.info("All posts processed.");
	}

	private List<Post> readPostFromUri() {
		try {
			return objectMapper.readValue(restTemplate.getForObject(postUri, String.class),
					new TypeReference<List<Post>>() {
					});
		} catch (IOException e) {
			log.error("Posts data structure has been change or data comes corrupted.");
			throw CorruptedPostsException.ofPosts().get();
		}
	}

	private void savePostJson(Post post) {
		try {
			objectMapper.writeValue(new File(postJsonDirectory + post.getId() + ".json"), post);
		} catch (JsonMappingException | JsonGenerationException e) {
			log.error("Post comes corrupted, stop processing {}", post.toString());
			throw CorruptedPostsException.ofPost(post).get();
		} catch (IOException e) {
			log.error("Error while saving post: {}", post.toString());
			throw SavingPostsException.ofPost(post).get();
		}
	}

	private void validateDirectory() {
		File directory = new File(postJsonDirectory);
		if (!directory.exists()) {
			log.info("Directory {} wasn't exist. Creating.", postJsonDirectory);
			directory.mkdirs();
		}
	}

}
