package com.networked.assets.jsonPlaceholder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Dominika Rakoczy
 */
@SpringBootApplication
public class JsonPlaceholderApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsonPlaceholderApplication.class, args);
	}

}
