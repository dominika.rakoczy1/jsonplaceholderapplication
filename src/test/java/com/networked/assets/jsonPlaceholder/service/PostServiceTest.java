package com.networked.assets.jsonPlaceholder.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.networked.assets.jsonPlaceholder.config.JacksonConfig;
import com.networked.assets.jsonPlaceholder.exception.CorruptedPostsException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { JacksonConfig.class })
public class PostServiceTest {

	private PostService postService;

	@Mock
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	private final static String testPostDirectory = "target/test/";

	@Before
	public void init() {
		postService = new PostService(restTemplate, objectMapper, testPostDirectory,
				"https://jsonplaceholder.typicode.com/posts");
	}

	@After
	public void clean() throws IOException {
		FileUtils.deleteDirectory(new File(testPostDirectory));
	}

	@Test
	public void shouldCorrectlyProcessedPost() {
		File testPostPathDirectory = new File(testPostDirectory);
		// check post directory before test, should be empty
		assertThat(testPostPathDirectory.list().length).isEqualTo(0);

		Mockito.when(restTemplate.getForObject("https://jsonplaceholder.typicode.com/posts", String.class))
				.thenReturn("[  {\"userId\": 1,\"id\": 17,\"title\": \"title1\",\"body\": \"body1\"}]");

		postService.saveAllPosts();

		assertThat(testPostPathDirectory.list().length).isEqualTo(1);
		assertThat(testPostPathDirectory.list()[0]).isEqualTo("17.json");

	}

	@Test(expected = CorruptedPostsException.class)
	public void shouldntProcessPostWithIncorrectBody() {
		Mockito.when(restTemplate.getForObject("https://jsonplaceholder.typicode.com/posts", String.class))
				.thenReturn("[  {not correct object}]");

		postService.saveAllPosts();
	}

}
