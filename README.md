### Uruchomienie aplikacji

Aby poprawnie uruchomić aplikację należy w pliku application.properties skonfigurować odpowiednie ustawienia, opisane poniżej:

- post.json.directory - miejsce umieszczenia przetworzonych postów
- post.uri - adres api z którego ma korzystać aplikacja

Uruchomienie zbudowanej aplikacji razem z odpowiadającą jej konfiguracją:

*java -Dspring.config.location=application.properties -jar jsonPlaceholder-0.0.1-SNAPSHOT.jar*

Po wystartowaniu aplikacji urchomiona zostanie ona na porcie 8080, dostępne metody restowe zobaczyć można przy pomocy Swaggera pod adresem: http://localhost:8080/swagger-ui.htm